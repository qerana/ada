<?php

/*
 * This file is part of ADA (abstract data access)
 * Copyright (C) 2019-20  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada\adapters;

/**
 * *****************************************************************************
 * Description of WsAdapter
 * *****************************************************************************
 * Implement AdapterInterface to consume an webserver to use in mappers
 * 
 * @author diemarc
 * *****************************************************************************
 */
class WsIPRAdapter implements AdapterInterface {

    private

    /** the conection to webservice */
            $WsResource,
            $api_entity,
            $method,
            $url_api,
            $url_string = '',
            $response = [];

    public function __construct(object $Resource, string $api_entity) {

        $this->WsResource = $Resource;
        $this->api_entity = $api_entity;
        $this->url_api = $this->WsResource->url . '' . strtolower($api_entity) . 'Api';
    }

    public function setMethod(string $method) {

        die("bibibi");
        $this->method = $method;
    }

    /**
     * Parse query conditions
     * @param array $conditions
     */
    private function _parseConditions(array $conditions = []) {
        // if isset conditions
        if (!empty($conditions)) {

            $c = 0;
            foreach ($conditions AS $field => $value):
                $c++;

                $this->url_string .= '&' . $field . '=' . $value;
            endforeach;
        }
    }

    /**
     * run query
     * @param array $conditions
     */
    private function _runQuery(array $conditions = []) {

        $this->_parseConditions($conditions);

        $url_api = $this->url_api . '&a=' . $this->method . $this->url_string . '&key=' . $this->WsResource->secret_key;


        $client = curl_init($url_api);

        curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
        $this->response = curl_exec($client);
        curl_close($client);
    }

    /**
     * 
     * @param string $method
     * @param array $conditions
     * @return type
     */
    public function callGet(string $method, array $conditions = []) {

        $this->method = $method;
        return $this->find($conditions);
    }

    /**
     * Call post
     * @param string $method
     * @param array $array
     */
    public function callPost(string $method, array $array) {

        // assign method
        $this->method = $method;

        //  set data to jsons
        $data = json_encode($array);

        // set utl 
        $url_api = $this->url_api . '&a=' . $this->method . $this->url_string . '&key=' . $this->WsResource->secret_key;

        // init curl
        $client = curl_init();
        
        
        curl_setopt($client, CURLOPT_URL, $url_api);
        curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($client, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//        curl_setopt($client, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($client, CURLOPT_GET, count($data));
        curl_setopt($client, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($client);

        // cerramos la conexion
        curl_close($client);

        return $response;
    }

    /**
     * Get query
     * @return type
     */
    public function getQuery() {
        return $this->_query;
    }

    /**
     * -------------------------------------------------------------------------
     * Finder xml query
     * ..........................................................................
     * @param array $conditions
     * @param array $options
     * @return type
     * 
     * multiple conditions example:
     * @url https://stackoverflow.com/questions/18547410/xpath-with-multiple-contains-on-different-elements
     * $Xpath->query('(//modules/module)[style[text()="sbadmin2"] and type[text()="qerapp"] ]');
     */
    public function find(array $conditions = [], array $options = []) {

        $this->_runQuery($conditions);

        $result = json_decode($this->response, true);

        return $result;
    }

    /**
     * -------------------------------------------------------------------------
     * Insert data into xml table
     * -------------------------------------------------------------------------
     * @param array $data
     */
    public function insert(array $data): int {
        
        
    }

    public function setResource(string $resource): void {
        
    }

    /**
     * UPDATE using xml load file
     * @param array $data
     * @param array $conditions
     */
    public function update1(array $data, array $conditions = array()) {

    }

    /**
     * Update xml node, USING domxpath
     * @param array $data
     * @param array $conditions
     */
    public function update(array $data, array $conditions = array()) {
    }

    /**
     * -------------------------------------------------------------------------
     * Delete a xml element
     * -------------------------------------------------------------------------
     * @param array $conditions
     */
    public function delete(array $conditions) {
    }

}

