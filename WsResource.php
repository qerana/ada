<?php

/*
 * This file is part of ADA (abstract data access)
 * Copyright (C) 2019  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada;

use QException\Exceptions;

/*
  |--------------------------------------------------------------------------
  | GET SINGLETON  TO WEBSERVICE RESOURE
  |--------------------------------------------------------------------------
  |
 */

class WsResource {

    private static $instance = null;
    public
            $secret_key,
            $url,
            $mode,
            $config;

    public function __construct() {
        //load qerana confguration file.
        $this->config = \Qerana\Configuration::singleton();
        require_once(__ROOTFOLDER__ . 'config/api_client_conf.php');

        try {

            // check if server is alive
            $fp = fsockopen($this->config->get('_serverhost_'), 80, $errno, $errstr, $this->config->get('_timeout_'));

            if (!$fp) {
                throw new \Exception('ApiServer is down or too busy:' . $errno . '-' . $errstr);
            }
            // init
            $this->secret_key = $this->config->get('_secret_key_');
            $request_protocol = ($this->config->get('_secure_')) ? 'https://' : 'http://';
            $this->url = $request_protocol . $this->config->get('_serverhost_') . '/' . $this->config->get('_urlapi_');
            $this->mode = $this->config->get('_friendly_');
        } catch (\Exception $ex) {
            Exceptions::ShowException('WSResourceDown:' . $this->config->get('_serverhost_'), $ex);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Singleton
     * -------------------------------------------------------------------------
     */
    public static function singleton() {
        if (self::$instance == null) {

            self::$instance = new self();
        }

        return self::$instance;
    }

}
