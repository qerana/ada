<?php

/*
 * This file is part of QeranaProject
 * Copyright (C) 2020-2021  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
  |--------------------------------------------------------------------------
  | Search engine
  |--------------------------------------------------------------------------
  |
  | Search in repositories fields
  |
 */

namespace Ada;

/**
 * Search service
 *
 * @author diemarc
 */
class AdaSearch {

    public
    /** @var int , total reg founded */
            $total = 0,
            /** @array the result */
            $results = [],
            /** The value for search */
            $string_to_search;
    protected
    /**  array ob object Repositories for run searching */
            $Repositories = [];

    public function __construct(string $value, array $Repositories) {

        $this->Repositories = $Repositories;
        $this->string_to_search = rawurldecode($value);
    }

    /**
     *  Run each respository to search and save result
     */
    public function search() {


        /**
         *  Foreach repository, get the conditions to use
         */
        foreach ($this->Repositories AS $k => $Repository):

            // the fields to search 
            foreach ($Repository['fields'] AS $field):

                $params[$k][] = [
                    $field => $this->string_to_search,
                    'operator' => (!isset($Repository['operator'])) ? 'LIKE' : $Repository['operator'],// operatitor = or like
                ];


            endforeach;

            // by default findall is the method, but you can use anothes specified in action
            $method = (!isset($Repository['action'])) ? 'findAll' : $Repository['action'];

            // if method is not findall,
            if ($method != 'findAll') {

                $result = $Repository['repo']->$method($Repository['condition'], $params[$k]);
            } 
            // find all method
            else {
                $result = $Repository['repo']->$method(
                        $params[$k], ['boolean' => true]
                );
            }

            $total_repo = count($result); // total result for all repos
            $this->total = $this->total + $total_repo;
            if ($total_repo > 0) {
                $this->results[$k]['source'] = $Repository['source'];
                $this->results[$k]['total_repo'] = $total_repo;
                $this->results[$k]['results'] = $result;
            }

        endforeach;

        return $this;
    }

    /**
     * parse results in json
     */
    public function toJson() {
        echo json_encode($this->results);
    }

}
