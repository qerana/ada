<?php

/*
 * This file is part of QeranaProject
 * Copyright (C) 2019-20  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada;

use Qerapp\qbasic\model\modeling\mapper\EntityXmlMapper AS EntityMapper;

/**
 * *****************************************************************************
 * Description of AdaRelation
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class EntityRelation
{

    public static
            $EntityRelatedMapper,
            $EntityRelatedRepository,
            $condition,
            $Entity;

    /**
     * find related entity and setup his namespace
     * @param type $related
     * @throws \InvalidArgumentException
     */
    public static function setEntityRelatedMapper($related)
    {

        // first get mapper related

        $EntityMapper = new EntityMapper;
        $EntityRelated = $EntityMapper->findEntity(ucfirst($related));

        if (empty($EntityRelated)) {
            throw new \InvalidArgumentException($related . ' Entity not found');
        }

        $Mapper = '\\' . $EntityRelated->Model->model_namespace . '\\mapper\\' . $EntityRelated->entity_name . 'Mapper';
        self::$EntityRelatedMapper = new $Mapper;
    }

    /**
     * Get related entity repository
     * @throws \InvalidArgumentException
     */
    public static function setRelatedRepository($related)
    {

        // get related entity
        $EntityMapper = new EntityMapper;
        $RelatedEntity = $EntityMapper->findEntity(ucfirst($related));

        if (empty($RelatedEntity)) {
            throw new \InvalidArgumentException($related . ' Entity not found');
        }

        $mapper_namespace = '\\' . $RelatedEntity->Model->model_namespace . '\\mapper\\' . $RelatedEntity->entity_name . 'Mapper';
        self::$EntityRelatedMapper = new $mapper_namespace;

        $repository_namespace = '\\' . $RelatedEntity->Model->model_namespace . '\\repository\\' . $RelatedEntity->entity_name . 'Repository';
        self::$EntityRelatedRepository = new $repository_namespace(self::$EntityRelatedMapper);
    }

    /**
     * Set the entity source
     * @param type $Entity
     */
    public static function setEntity($Entity)
    {
        self::$Entity = $Entity;
    }

    /**
     * set condition to use in mapper
     * @param type $condition
     */
    public static function setCondition($condition)
    {
          // flush condition, always reset all conditions
        self::$condition = [];
        
        if (is_array($condition)) {
            foreach ($condition AS $field):
                self::$condition[] = [$field => self::$Entity->$field];
            endforeach;
        }else {
            self::$condition = [$condition => self::$Entity->$condition];
        }
    }

    /**
     * one to one relationship
     * @param object $Entity
     * @param string $related
     * @param type $fields
     * @return type
     */
    public static function hasOne(object $Entity, string $related, $fields,$callback = null, $repository = null)
    {
        if (is_null($repository)) {
            try {
                self::setRelatedRepository($related);
            } catch (\Exception $exc) {
                \QException\Exceptions::ShowException('Ada-Relation-HasOne', $exc);
            }

            self::setEntity($Entity);
            self::setCondition($fields);
        }else{
            self::$EntityRelatedRepository = $repository;
        }

        if(is_null($callback)){
            return self::$EntityRelatedRepository->findAll(self::$condition,['fetch' => 'one']);
        }else{
             return self::$EntityRelatedRepository->$callback(self::$condition);
        }
        

       
    }

    /**
     * one to one relationship
     * @param object $Entity
     * @param string $related
     * @param type $fields
     * @return type
     */
    public static function hasMany(object $Entity, string $related, $fields,$callback = null, $repository = null)
    {

         if (is_null($repository)) {
            try {
                self::setRelatedRepository($related);
            } catch (\Exception $exc) {
                \QException\Exceptions::ShowException('Ada-Relation-HasOne', $exc);
            }

            self::setEntity($Entity);
            self::setCondition($fields);
        }else{
            self::$EntityRelatedRepository = $repository;
        }

        if(is_null($callback)){
            return self::$EntityRelatedRepository->findAll(self::$condition);
        }else{
             return self::$EntityRelatedRepository->$callback(self::$condition);
        }
        
    }

}
