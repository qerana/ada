<?php

/*
 * This file is part of ADA (abstract data access)
 * Copyright (C) 2019  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada;
use QException\Exceptions;

/*
  |--------------------------------------------------------------------------
  | GET SINGLETON CONECTION TO PDO A SQLITE DB
  |--------------------------------------------------------------------------
  |
 */

use PDO;

class SqlLitePDO extends PDO
{

    private static $instance_lite = null;
    private static $db_file;

    public function __construct($db = '')
    {

        if (empty($db)) {
            $config = \Qerana\Configuration::singleton();
            self::$db_file = $config->get('_sqlitedb_');
        } else {
            self::$db_file = $db;
        }

        
        /**
         * ---------------------------------------------------------------------
         * PDO options
         * ---------------------------------------------------------------------
         */
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // error exception mode
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'" // utf8 always
        ];

        $db_path = realpath(self::$db_file);
        if (empty($db_path)) {
            
           Exceptions::showError('ADA/Error.Connection', 'sqlite.database.file not found (' . self::$db_file . ')');
        }

        try {
            parent::__construct('sqlite:' . $db_path, null, null, $options);
        } catch (\Exception $ex) {
            Exceptions::ShowException('ADA/Error.Connection', $ex);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Singleton
     * -------------------------------------------------------------------------
     */
    public static function singleton($db = '')
    {
        if (self::$instance_lite == null) {

            self::$instance_lite = new self($db);
        }

        return self::$instance_lite;
    }

}
