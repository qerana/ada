<?php

/*
 *  * This file is part of ADA (abstract data access)
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada\filter;

use Ada\adapters\AdapterInterface;

/**
 * *****************************************************************************
 * Interface for adaFilter
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
interface AdaFilterInterface {

    public function set_ofset($ofset = '');

    public function set_filter_name(string $filter): void;

    public function set_rows_per_page(int $per_page);

    public function set_preserve_data(bool $preserve_data);

    public function set_query(string $query);

    public function set_adapter(AdapterInterface $Adapter);

    public function set_condition(array $conditions = []);
    
    public function set_order_by(string $order_by);
    
    public function set_order_direction(string $order_direction);
    
    public function set_debug(bool $debug);
    
    public function set_mapper(object $Mapper);
    
    public function set_store_all(bool $store_alls);
    
    
    public function get_ofset();
    
    public function get_filter_name();
    
    public function get_rows_per_page();
    
    public function get_query();
    
    public function get_adapter();
    
    public function get_condition();
    
    public function get_order_by();
    
    public function get_order_direction();
    
    public function get_debug();
    
    public function get_store_all();
    
    public function get_mapper();

    public function get();
}

