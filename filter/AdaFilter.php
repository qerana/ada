<?php

/*
 * This file is part of QeranaProject
 * Copyright (C) 2020-2021  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada\filter;

use Ada\adapters\AdapterInterface,
    Qerana\core\QSession,
    Ada\filter\AdaFilterInterface;

/*
  |--------------------------------------------------------------------------
  | ADA Filter
  |--------------------------------------------------------------------------
  |
  | Perform filters, pagination, from a sql query
  | @author diemarc
 */

class AdaFilter {

    public
    // @int  total rows
            $total_rows,
            // result data filtered
            $data_filtered,
            // order by 
            $order_by,
            // order request from frontend
            $order_request,
            // order direction
            $order_direction,
            // format to parse the data filtered (json,array,xls,pdf,docx)
            $format = 'JSON',
            // if you want to add statemennt similar to group by at the end of query
            $query_mutator = null,
            // @boolean debug
            $debug = false,
            $orders = [];
    protected

    // @string the query to apply in filter
            $query_filter,
            // @array query conditions
            $conditions = [],
            // @bool if yo want to preserve the data conditions (Session be used)
            $preserve_data = true,
            // params filter, this will stored in session
            $params_filter = [],
            // @string filter name, used to store session prefix, random string
            $filter_name,
            // @int rows per pages to show
            $rows_per_page,
            // @int  ofset, limit ofset perpage
            $ofset,
            // @array  the binds of conditions
            $filter_binds = [],
            // @string field of data to apply in conditions
            $date_field,
            // @string name of date field received by request
            $date_name = 'df-date',
            // store data filtered without ofset, for exportation purpose
            $store_no_ofset = false,
            // @string quey without ofset
            $query_no_ofset;
    private
    /**
     *  @object PDO Adapter 
     */
            $Adapter,
            $Filter,
            // @int next ofset value
            $next_val,
            // @int prev ofset value
            $prev_val,
            // @string default request to get params (_POST,_GET)
            $request_method = INPUT_GET;

    public function __construct(AdaFilterInterface $Filter) {


        $this->Filter = $Filter;

        // filter name
        $this->filter_name = $this->Filter->filter_name . '-';

        // rows per page
        $this->setRowsPerPage($this->Filter->rows_per_page);

        //  preserve data in sessions
        $this->setPreserveData($this->Filter->preserve_data);

        // order request
        $this->order_request = filter_input($this->request_method, 'df-order-by');
        $this->order_by = $this->order_request;
        $this->order_direction = $this->Filter->order_direction;

        // create adapter pdo instance
        $this->Adapter = $this->Filter->Adapter;

        // set debug

        $this->debug = $this->Filter->debug;

        // store data without ofset
        $this->store_no_ofset = $this->Filter->store_all;


        $this->query_mutator = $this->Filter->query_mutator;

        // set query
        $this->query_filter = $this->Filter->query;

        // set all conditions
        $this->setAllConditions();
    }

    /**
     * -------------------------------------------------------------------------
     * set rows per page
     * -------------------------------------------------------------------------
     * @param int $rows
     */
    private function setRowsPerPage(int $rows): void {
        $this->rows_per_page = filter_var($rows, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * -------------------------------------------------------------------------
     * Set preserve-data
     * -------------------------------------------------------------------------
     * @param bool $value
     */
    private function setPreserveData(bool $value) {
        $this->preserve_data = $value;

        new QSession();
        if (!$this->preserve_data) {

            // unset all stored session
            $this->unsetAllFilterParameters();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Set order by
     * -------------------------------------------------------------------------
     */
    private function setupOrderBy() {

        if ($this->preserve_data) {

            // search order
            $orderby = $this->searchOrderBy();
            $orderdirection = $this->searchOrderDirection();

            if (!empty($orderby)) {
                $this->query_filter .= ' ORDER BY ' . $orderby . ' ' . $orderdirection . ' ';
            }
        } else {
            if (isset($this->order_by)) {
                $this->query_filter .= ' ORDER BY ' . $this->order_by . ' ';
                $this->query_filter .= filter_input($this->request_method, 'df-order-direction');
            }
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Search and set order by
     * -------------------------------------------------------------------------
     * @return type
     */
    private function searchOrderBy() {

        if (!empty($this->order_by)) {

            $order = $this->order_by;
            // create session
            $_SESSION[$this->filter_name . 'orderby'] = $order;
        } else {

            // searchin sessions
            if (isset($_SESSION[$this->filter_name . 'orderby'])) {
                $order = (isset($_SESSION[$this->filter_name . 'orderby'])) ? $_SESSION[$this->filter_name . 'orderby'] : '';
            }
        }
        $order = (isset($_SESSION[$this->filter_name . 'orderby'])) ? $_SESSION[$this->filter_name . 'orderby'] : '';

        return $order;
    }

    /**
     * -------------------------------------------------------------------------
     * Search and set order direction
     * -------------------------------------------------------------------------
     * @return type
     */
    private function searchOrderDirection() {

        $this->order_direction = filter_input($this->request_method, 'df-order-direction');

        if (!empty($this->order_direction)) {

            $order_direction = $this->order_direction;
            // create session
            $_SESSION[$this->filter_name . 'orderdirection'] = $order_direction;
        } else {

            // searchin sessions
            if (isset($_SESSION[$this->filter_name . 'orderdirection'])) {
                $order_direction = (isset($_SESSION[$this->filter_name . 'orderdirection'])) ? $_SESSION[$this->filter_name . 'orderdirection'] : '';
            }
        }
        $order_direction = (isset($_SESSION[$this->filter_name . 'orderdirection'])) ? $_SESSION[$this->filter_name . 'orderdirection'] : '';

        return $order_direction;
    }

    /**
     * -------------------------------------------------------------------------
     * Set condition filter
     * -------------------------------------------------------------------------
     * @param type $condition
     * @param type $operator
     */
    public function setCondition(array $condition) {

        //check if reset is called
        if (filter_input($this->request_method, 'df_reset') === '1') {
            $this->unsetAllFilterParameters();
        }


        $condition_name_value = $this->getParam($condition['name']);

        if (!empty($condition_name_value)) {
            $bind_name = ':' . $condition['name'];
            $this->filter_binds[$bind_name] = $condition_name_value;

            if (!empty($condition['condition'])) {

                if (!is_array($condition['condition'])) {
                    $this->query_filter .= ' ' . $condition['condition'];
                } else {

                    foreach ($condition['condition'] AS $key => $value) {

                        if (($key == $condition_name_value) AND ! empty($value)) {
                            $this->query_filter .= ' ' . $value;
                        }
                    }
                }
            }
        }
    }

    /**
     * -------------------------------------------------------------------------
     * run data filtered
     * -------------------------------------------------------------------------
     * @var int, ofset the ofset to filter
     * @return type
     */
    public function filter() {


        (!is_null($this->query_mutator)) ? $this->query_filter .= $this->query_mutator : '';

        // total rows query without ofsets
        $this->setTotalRows();
        // set the ofset
        $this->setOfset($this->Filter->ofset);

        // set order by
        $this->setupOrderBy();



        // apply limits and ofset
        $this->query_filter .= ' LIMIT ' . $this->ofset . ',' . $this->rows_per_page;

        // value next/prev

        $prev_ofset = $this->ofset - $this->rows_per_page;

        $this->next_val = $this->ofset + $this->rows_per_page;
        $this->prev_val = ($prev_ofset < 0) ? 0 : $prev_ofset;



        $FilterResult = $this->Adapter->selectByQuery($this->query_filter, $this->filter_binds);
        // data array
        $this->data_filtered = [
            'total' => $this->total_rows,
            'ofset' => $this->ofset,
            'per_page' => $this->rows_per_page,
            'next' => $this->next_val,
            'prev' => $this->prev_val,
            'params' => $this->params_filter,
            'Result' => $FilterResult,
            'Data' => $this->query_no_ofset
        ];

        return $this;
    }

    /**
     *  set all conditions
     */
    public function setAllConditions() {

        foreach ($this->Filter->condition AS $condition):

            $this->setCondition($condition);
        endforeach;
    }

    /**
     * -------------------------------------------------------------------------
     * Total rows
     * -------------------------------------------------------------------------
     */
    public function setTotalRows() {

        $query_result = $this->Adapter->selectByQuery($this->query_filter, $this->filter_binds);

        if ($this->store_no_ofset) {
            $this->query_no_ofset = $query_result;
        }
        $this->total_rows = count($query_result);
    }

    /**
     * -------------------------------------------------------------------------
     * Set ofset
     * -------------------------------------------------------------------------
     * @param int $ofset_var
     */
    public function setOfset($ofset_var = '') {

        // if preserve data
        if ($this->preserve_data) {


            if (is_numeric($ofset_var)) {

                $ofset = $ofset_var;
                $_SESSION[$this->filter_name . 'ofset'] = $ofset;
            } else {
                if (isset($_SESSION[$this->filter_name . 'ofset'])) {

                    $ofset = $_SESSION[$this->filter_name . 'ofset'];
                }
            }
            $ofset = (isset($_SESSION[$this->filter_name . 'ofset'])) ? $_SESSION[$this->filter_name . 'ofset'] : '';
        } else {
            $ofset = $ofset_var;
        }


        if ($ofset <= 0) {
            $this->ofset = 0;
        } else {

            if ($ofset >= $this->total_rows) {
                $this->ofset = $this->total_rows;
            } else {
                $this->ofset = $ofset;
            }
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Set param
     * -------------------------------------------------------------------------
     * @param string $param_name
     * @return type
     */
    public function getParam(string $param_name) {
        if ($this->preserve_data) {
            // comprobamos que ha venido como paremetro
            if (!empty(filter_input($this->request_method, $param_name))) {
                $string = trim(filter_input($this->request_method, $param_name));
                $_SESSION[$this->filter_name . $param_name] = $string;
            }

            // sino vemos como sesion
            else {
                if (isset($_SESSION[$this->filter_name . $param_name])) {
                    $string = $_SESSION[$this->filter_name . $param_name];
                }
            }
            $param = (isset($_SESSION[$this->filter_name . $param_name])) ? $_SESSION[$this->filter_name . $param_name] : '';
        } else {
            $param = trim(filter_input($this->request_method, $param_name));
        }


        (!empty($param)) ? $this->params_filter[$param_name] = $param : '';

        return $param;
    }

    /**
     * -------------------------------------------------------------------------
     * Get data filtered
     * -------------------------------------------------------------------------
     */
    public function get() {

        // if debug is true then run debug
        ($this->debug) ? $this->runDebug() : '';
        return $this;
    }

    /**
     * -------------------------------------------------------------------------
     * Get in array
     * -------------------------------------------------------------------------
     */
    public function inArray() {
        return $this->data_filtered;
    }

    /**
     * -------------------------------------------------------------------------
     * Parse to format setted the data filtered
     * -------------------------------------------------------------------------
     */
    public function inJson() {
        \helpers\Utils::parseToJson($this->data_filtered);
    }

    /**
     * -------------------------------------------------------------------------
     * Print debug controls
     * -------------------------------------------------------------------------
     */
    private function runDebug() {

        ini_set('display_errors', 1);
        error_reporting(-1);
        error_reporting(E_ALL);


        echo '<h1>QueryFilter-Debuging</h1>';
        echo '<ol>';
        echo '<li>name:' . $this->filter_name . '</li>';
        echo '<li>rows:' . $this->rows_per_page . '</li>';
        echo '<li>current_ofset:' . $this->ofset . '</li>';
        echo '<li>prev_ofset:' . $this->prev_val . '</li>';
        echo '<li>next_ofset:' . $this->next_val . '</li>';
        echo '<li>preserve-data:' . $this->preserve_data . '</li>';
        echo '</ol>';
        echo '<h3>Query</h3>';
        echo $this->query_filter;
        if (isset($this->date_field)) {
            echo '<h3>DateFilterField setted</h3>';
            echo $this->date_field . ' (' . $this->date_name . ')';
        }


        echo '<h4>QueryConditions</h4>';
        echo '<pre>';
        print_r($this->conditions);
        echo '<h4>QueryBinds</h4>';

        print_r($this->filter_binds);
        echo '<h4>Parameters filter</h4>';
        print_r($this->params_filter);
        echo '<h4>Sessions</h4>';

        foreach ($_SESSION AS $n_s => $v) {
            if (substr($n_s, 0, strlen($this->filter_name)) == $this->filter_name) {
                echo 'SESSION[' . $n_s . '] = ' . $v . '<br>';
            }
        }


        echo '<h2>Result</h2>';
        print_r($this->data_filtered);

        die();
    }

    /**
     * -------------------------------------------------------------------------
     * Unset all session and params filter
     * -------------------------------------------------------------------------
     */
    private function unsetAllFilterParameters() {

        foreach ($_SESSION AS $n_s => $v) {
            if (substr($n_s, 0, strlen($this->filter_name)) == $this->filter_name) {
                // echo 'unset ' . $n_s . '<br>';
                unset($_SESSION[$n_s]);
                unset($this->params_filter[$n_s]);
                //$this->_params_filter = ' ';
            }
        }
    }

}
