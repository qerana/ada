<?php

/*
 * This file is part of QeranaProject
 * Copyright (C) 2020-2021  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada\filter;

use Ada\filter\AdaFilter,
    Ada\adapters\PDOAdapter,
    Ada\adapters\AdapterInterface,
    SimpleXLSXGen,
    Ada\EntityManager;

/**
 * ------------------------------------------------------------------------------
 * AbstractAdaFilter
 * ------------------------------------------------------------------------------
 * Abstract implementation of AdaFilterInterface, 
 * Set the basic initial setup for AdaFilter, all filter extends of this class
 *
 * @author diemarc
 */
class AbstractAdaFilter extends EntityManager implements AdaFilterInterface {

    private
            $AdaFilter;
    protected

    // @string, filter name, used to stores session vars
            $_filter_name,
            // @int, rows per page to show on filter page
            $_rows_per_page = 20,
            // @boolean, store filter parameters on Session variables
            $_preserve_data = true,
            // @boolean, debug true to set all query, parameters stored, etc
            $_debug = false,
            // @string, query to run
            $_query,
            //query mutator, para usar group by o having
            $_query_mutator,
            // @int, ofset
            $_ofset,
            // @Object, implementacion of AdapterInteface
            $_adapter,
            // @array, conditions to apply on query
            $_condition = [],
            //string, order by to apply in query
            $_order_by,
            // @string order direction
            $_order_direction,
            // @boolean, true to store on array all data wihout ofset
            $_store_all = false,
            // @object mapper
            $_mapper,
            // @array of result 
            $_result,
            // extras to filter
            $_extras,
            // @xls generation
            $_xlsx = false;
    public
            $Collection = [];

    public function __construct($ofset = '', $Mapper = null) {

        $this->_ofset = $ofset;

        if (!is_null($Mapper)) {
            $this->set_mapper($Mapper);
        }

        // crear PDO Adapter new object, yo can override this, in your own derived class
    }

    /*     * *
     * Create a adapter if not exists 
     */

    private function createAdapter() {

        if (!$this->_adapter instanceof AdapterInterface) {
            $this->Adapter = new PDOAdapter(\Ada\MysqlPDO::singleton());
        }
    }

    /**
     *  Get the data filtered
     */
    public function get() {

        try {
            $this->AdaFilter = new AdaFilter($this);
            $this->AdaFilter->filter()->get();

            // sete the result
            $this->_result = ($this->_store_all OR $this->_xlsx) ? $this->AdaFilter->data_filtered['Data'] : $this->AdaFilter->data_filtered['Result'];


            return $this;
        } catch (\Throwable $ex) {

            \QException\Exceptions::ShowException('AdaFilter.Get.#Num:' . rand(), $ex);
        }
    }


 /**
     * Agrega elementos extras al json
     * @param type $key
     * @param type $value
     */
    public function addExtra($key, $value) {
        
        if(is_object($this->AdaFilter)){
             $this->AdaFilter->data_filtered[$key] = $value;
        }else{
            throw new \InvalidArgumentException('Adafilter no existe');
        }
        
       
    }


    /**
     * return in array
     * @return type
     */
    public function inArray() {
        // if exists a mapper objetct
        if (is_object($this->Mapper)) {

            // mapt in object entity
            (empty($this->Collection)) ? $this->map() : '';
        }
    }

    /**
     * ----------------------------------------------------------------------------
     * Parse in xlsx, SimpleXLSX lib required, composer require
     * ----------------------------------------------------------------------------
     * @param type $ofset
     */
    public function inXlsx() {


        // titles, of first result
        $titles = array_keys($this->_result[0]);

        // array to parse to xls
        $data_xls = [
            $titles,
        ];
        foreach ($this->_result AS $row):
            $data_xls[] = $row;
        endforeach;

        $xlsx = SimpleXLSXGen::fromArray($data_xls);
        $xlsx->downloadAS($this->_filter_name . '.xlsx');
    }

    /**
     * ----------------------------------------------------------------------------
     * parse in json
     * ----------------------------------------------------------------------------
     * @param type $ofset
     */
    public function inJson() {

        // if exists a mapper objetct
        if (is_object($this->Mapper)) {

            // mapt in object entity
            (empty($this->Collection)) ? $this->map() : '';
        }
        $this->AdaFilter->inJson();
    }

    /**
     * ----------------------------------------------------------------------------
     * SMapping to object
     * ----------------------------------------------------------------------------
     * @param type $ofset
     */
    public function map() {


        try {

            // create a entity Collection
            foreach ($this->_result AS $Result):
                $this->Collection[] = $this->Mapper->createEntity($Result);
            endforeach;

            // replace the result array, with the collection entitu
            $this->AdaFilter->data_filtered['Result'] = $this->Collection;

            return $this;
        } catch (\Throwable $ex) {
            \QException\Exceptions::ShowException('AdaFilter.MapEntity.#Num:' . rand(), $ex);
        }
    }

    /**
     * ----------------------------------------------------------------------------
     * SETTERS
     * ----------------------------------------------------------------------------
     * @param type $ofset
     */

    /**
     * Set mapper  and set the adapter
     * @param object $Mapper
     */
    public function set_mapper(object $Mapper) {

        $this->_mapper = $Mapper;
        $this->_adapter = $this->_mapper->getAdapter();
    }

    // set query and create adapter if not created
    public function set_query(string $query) {
        $this->createAdapter();
        $this->_query = $query;
    }
    public function set_query_mutator(string $query) {
        
        $this->_query_mutator = $query;
    }

    public function set_ofset($ofset = '') {
        $this->_ofset = $ofset;
    }

    public function set_adapter(\Ada\adapters\AdapterInterface $Adapter) {
        $this->_adapter = null;
        $this->_adapter = $Adapter;
    }

    public function set_condition(array $conditions = array()) {
        $this->_condition[] = $conditions;
    }

    public function set_filter_name(string $filter): void {
        $this->_filter_name = $filter;
    }

    public function set_preserve_data(bool $preserve_data) {
        $this->_preserve_data = $preserve_data;
    }

    public function set_rows_per_page(int $per_page) {
        $this->_rows_per_page = $per_page;
    }

    public function set_debug(bool $debug) {
        $this->_debug = $debug;
    }

    public function set_order_by($_order_by) {
        $this->_order_by = $_order_by;
    }

    public function set_order_direction($_order_direction) {
        $this->_order_direction = $_order_direction;
    }

    public function set_store_all(bool $_store_all) {
        $this->_store_all = $_store_all;
    }

    public function set_xlsx(bool $_xlsx) {

        $this->_xlsx = $_xlsx;
        $this->_store_all = ($this->_xlsx) ? true : false;
    }

    /**
     * ----------------------------------------------------------------------------
     * GETTES
     * ----------------------------------------------------------------------------
     */
    public function get_filter_name() {
        return $this->_filter_name;
    }

    public function get_rows_per_page() {
        return $this->_rows_per_page;
    }

    public function get_preserve_data() {
        return $this->_preserve_data;
    }

    public function get_debug() {
        return $this->_debug;
    }

    public function get_query() {
        return $this->_query;
    }

    public function get_ofset() {
        return $this->_ofset;
    }

    public function get_adapter() {
        return $this->_adapter;
    }

    public function get_condition() {
        return $this->_condition;
    }

    public function get_order_by() {
        return $this->_order_by;
    }

    public function get_order_direction() {
        return $this->_order_direction;
    }

    public function get_store_all() {
        return $this->_store_all;
    }

    public function get_mapper() {
        return $this->_mapper;
    }

    public function get_xlsx() {
        return $this->_xlsx;
    }

}
